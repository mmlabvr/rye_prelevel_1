﻿using UnityEngine;
using System.Collections;

public class CameraZoom : MonoBehaviour
{
    private Transform m_cameraMostLeftPosition;
    private Transform m_cameraMostRightPosition;

    private float m_cameraMinSize;
    private float m_cameraMaxSize;
    private float m_cameraSaveSize;

	void Start ()
    {
        m_cameraMostLeftPosition = GetComponent<Camera>().GetComponent<CameraViewHandled>().m_mostLeftPosition;
        m_cameraMostRightPosition = GetComponent<Camera>().GetComponent<CameraViewHandled>().m_mostRightPosition;

        m_cameraMinSize = 2f;
        m_cameraMaxSize = 5f;
        m_cameraSaveSize = GetComponent<Camera>().orthographicSize;
	}

    public void ZoomIn()
    {
        if (GetComponent<Camera>().orthographicSize == m_cameraSaveSize && GetComponent<Camera>().orthographicSize > m_cameraMinSize)
        {
            SetCameraEdgePosition(Round(GetComponent<Camera>().orthographicSize - 0.5f, 1));
            StartCoroutine(Zoom(Round(Mathf.Max(GetComponent<Camera>().orthographicSize - 0.5f, m_cameraMinSize), 1), -0.02f));
            m_cameraSaveSize = Round(GetComponent<Camera>().orthographicSize - 0.5f, 1);
        }
    }

    public void ZoomOut()
    {
        if (GetComponent<Camera>().orthographicSize == m_cameraSaveSize && GetComponent<Camera>().orthographicSize < m_cameraMaxSize)
        {
            SetCameraEdgePosition(Round(GetComponent<Camera>().orthographicSize + 0.5f, 1));
            StartCoroutine(Zoom(Round(Mathf.Min(GetComponent<Camera>().orthographicSize + 0.5f, m_cameraMaxSize), 1), 0.02f));
            m_cameraSaveSize = Round(GetComponent<Camera>().orthographicSize + 0.5f, 1);
        }
    }

    IEnumerator Zoom(float zoomSize, float step)
    {
        while (Mathf.Abs(GetComponent<Camera>().orthographicSize - zoomSize) >= 0.01f)
        {
            yield return new WaitForSeconds(0.005f);
            GetComponent<Camera>().orthographicSize += step;
        }

        GetComponent<Camera>().orthographicSize = zoomSize;
        if ((GetComponent<Camera>().transform.position - m_cameraMostLeftPosition.position).sqrMagnitude <= (GetComponent<Camera>().transform.position - m_cameraMostRightPosition.position).sqrMagnitude)
        {
            while ((GetComponent<Camera>().transform.position - m_cameraMostLeftPosition.position).sqrMagnitude > 0.0001f)
            {
                yield return new WaitForSeconds(0.005f);
                GetComponent<Camera>().transform.position = Vector3.Lerp(GetComponent<Camera>().transform.position, m_cameraMostLeftPosition.position, Time.deltaTime);
            }

            GetComponent<Camera>().transform.position = m_cameraMostLeftPosition.position;
        }
        else
        {
            while ((GetComponent<Camera>().transform.position - m_cameraMostRightPosition.position).sqrMagnitude > 0.0001f)
            {
                yield return new WaitForSeconds(0.005f);
                GetComponent<Camera>().transform.position = Vector3.Lerp(GetComponent<Camera>().transform.position, m_cameraMostRightPosition.position, Time.deltaTime);
            }

            GetComponent<Camera>().transform.position = m_cameraMostRightPosition.position;
        }
    }


    public IEnumerator AutoZoomIn()
    {
        yield return new WaitForSeconds(3f);
        while(GetComponent<Camera>().orthographicSize > m_cameraMinSize)
        {
            ZoomIn();
            yield return null;
        }
    }

    void SetCameraEdgePosition(float cameraSize)
    {
        if (cameraSize == 2f)
        {
            m_cameraMostLeftPosition.position = new Vector3(4.2f, -0.7f) + Vector3.back * 10;
            m_cameraMostRightPosition.position = new Vector3(11f, -0.7f) + Vector3.back * 10;
        }
        else if (cameraSize == 2.5f)
        {
            m_cameraMostLeftPosition.position = new Vector3(5.2f, -0.7f) + Vector3.back * 10;
            m_cameraMostRightPosition.position = new Vector3(11f, -0.7f) + Vector3.back * 10;
        }
        else if (cameraSize == 3f)
        {
            m_cameraMostLeftPosition.position = new Vector3(6.0f, -0.7f) + Vector3.back * 10;
            m_cameraMostRightPosition.position = new Vector3(11f, -0.7f) + Vector3.back * 10;
        }
        else if (cameraSize == 3.5f)
        {
            m_cameraMostLeftPosition.position = new Vector3(6f, -0.25f) + Vector3.back * 10;
            m_cameraMostRightPosition.position = new Vector3(10f, -0.25f) + Vector3.back * 10;
        }
        else if (cameraSize == 4)
        {
            m_cameraMostLeftPosition.position = new Vector3(6.5f, 0f) + Vector3.back * 10;
            m_cameraMostRightPosition.position = new Vector3(9f, 0f) + Vector3.back * 10;
        }
        else if (cameraSize == 4.5f)
        {
            m_cameraMostLeftPosition.position = new Vector3(7.5f, 0.25f) + Vector3.back * 10;
            m_cameraMostRightPosition.position = new Vector3(8.2f, 0.25f) + Vector3.back * 10;
        }
        else if (cameraSize == 5f)
        {
            m_cameraMostLeftPosition.position = new Vector3(8f, 0.7f) + Vector3.back * 10;
            m_cameraMostRightPosition.position = new Vector3(8f, 0.7f) + Vector3.back * 10;
        }
    }

    float Round(float number, int digit)
    {
        number = Mathf.Round(number * Mathf.Pow(10f, digit)) / Mathf.Pow(10f, digit);

        return number;
    }
}

﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
    private float m_shakeIntensity;
    private float m_shakeDecay;

    private Vector3 m_originPosition;
    private Quaternion m_originRotation;

    void Update()
    {
        if (m_shakeIntensity > 0)
        {
            transform.position = m_originPosition + Random.insideUnitSphere * m_shakeIntensity;
            transform.rotation = new Quaternion(
                                                    m_originRotation.x + Random.Range(-m_shakeIntensity, m_shakeIntensity) * .2f,
                                                    m_originRotation.y + Random.Range(-m_shakeIntensity, m_shakeIntensity) * .2f,
                                                    m_originRotation.z + Random.Range(-m_shakeIntensity, m_shakeIntensity) * .2f,
                                                    m_originRotation.w + Random.Range(-m_shakeIntensity, m_shakeIntensity) * .2f
                                               );
            m_shakeIntensity -= m_shakeDecay;
        }
    }

    public void Shake()
    {
        m_shakeIntensity = 0.3f;
        m_shakeDecay = 0.03f;

        m_originPosition = transform.position;
        m_originRotation = transform.rotation;
    }
}

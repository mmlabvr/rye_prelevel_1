﻿using UnityEngine;
using System.Collections;
using RyeSupport;

public class CameraViewHandled : MonoBehaviour
{
    public Transform m_mostLeftPosition;
    public Transform m_mostRightPosition;

    public GameObject m_slingshot;

    [HideInInspector]
    public CameraState m_state;
    [HideInInspector]
    public Direction m_direction;
    [HideInInspector]
    public Direction m_previousDirection;

    [HideInInspector]
    public float m_speed;
    private Vector3 m_previousPosition;

    private bool m_isClickOnBird;

    void Start()
    {
        m_state = CameraState.AUTO_MOVING;
        m_direction = Direction.LEFT;
        m_previousDirection = Direction.NONE;

        m_speed = 5f;
        m_previousPosition = Vector3.zero;
    }

    void Update()
    {
        GameObject bird = m_slingshot.GetComponent<Slingshot>().m_bird;
        GameObject previousBird = m_slingshot.GetComponent<Slingshot>().m_previousBird;

        switch (m_state)
        {
            case CameraState.IDLE:
                if (Input.GetMouseButtonDown(0))
                {
                    m_previousPosition = Input.mousePosition;

                    if (bird != null && bird.GetComponent<CircleCollider2D>() == Physics2D.OverlapPoint(Camera.main.ScreenToWorldPoint(m_previousPosition)))
                        m_isClickOnBird = true;
                    else
                        m_isClickOnBird = false;

                    /////////////////////////////////////////////////////////////////////////////////
                    //Change to Drag state when not click on bird
                    if (!m_isClickOnBird)
                        m_state = CameraState.MOVING_UNDER_CONTROL;
                    /////////////////////////////////////////////////////////////////////////////////
                }
                break;

            case CameraState.MOVING_UNDER_CONTROL:
                if (Input.GetMouseButton(0))
                {
                    Vector3 mousePosition = Input.mousePosition;

                    /////////////////////////////////////////////////////////////////////////////////
                    //Drag Camera
                    float delta = Camera.main.ScreenToWorldPoint(mousePosition).x - Camera.main.ScreenToWorldPoint(m_previousPosition).x;
                    Vector3 newPosition = new Vector3(transform.position.x - delta, transform.position.y, transform.position.z);

                    if (newPosition.x < m_mostLeftPosition.position.x)
                        newPosition.x = m_mostLeftPosition.position.x;
                    else if (newPosition.x > m_mostRightPosition.position.x)
                        newPosition.x = m_mostRightPosition.position.x;

                    transform.position = newPosition;
                    /////////////////////////////////////////////////////////////////////////////////



                    /////////////////////////////////////////////////////////////////////////////////
                    //Set drag speed and move direction
                    m_speed = delta * 10;
                    if (m_speed < -0.005f)
                    {
                        m_speed *= -1;
                        m_direction = Direction.RIGHT;
                        m_previousDirection = Direction.NONE;
                    }
                    else if (m_speed > 0.005f)
                    {
                        m_direction = Direction.LEFT;
                        m_previousDirection = Direction.NONE;
                    }
                    else
                    {
                        if (transform.position.x == m_mostLeftPosition.position.x || transform.position.x == m_mostRightPosition.position.x)
                        {
                            if (m_previousDirection == Direction.NONE)
                                m_previousDirection = m_direction;
                            m_direction = Direction.NONE;
                        }
                    }
                    /////////////////////////////////////////////////////////////////////////////////

                    m_previousPosition = mousePosition;
                }

                else if (Input.GetMouseButtonUp(0))
                {
                    if (m_direction == Direction.LEFT)
                    {
                        if (transform.position.x > m_mostLeftPosition.position.x && m_speed < 0.5f)
                            m_speed = 5f;
                    }
                    else if (m_direction == Direction.RIGHT)
                    {
                        if (transform.position.x < m_mostRightPosition.position.x && m_speed < 0.5f)
                            m_speed = 5f;
                    }

                    m_state = CameraState.AUTO_MOVING;
                }
                break;

            case CameraState.AUTO_MOVING:
                if (m_direction == Direction.LEFT)
                    transform.position = Vector3.Lerp(transform.position, m_mostLeftPosition.position, m_speed / 2 * Time.deltaTime);
                else if (m_direction == Direction.RIGHT)
                    transform.position = Vector3.Slerp(transform.position, m_mostRightPosition.position, m_speed / 2 * Time.deltaTime);

                if ((transform.position- m_mostLeftPosition.position).magnitude <= 0.05f || (transform.position - m_mostRightPosition.position).magnitude <= 0.05f)
                {
                    m_previousDirection = m_direction;
                    m_direction = Direction.NONE;
                    m_speed = 0f;

                    m_state = CameraState.IDLE;
                }

                if (GameState.m_state != PlayState.Win && Input.GetMouseButtonDown(0))
                {
                    m_speed = 0f;

                    m_previousPosition = Input.mousePosition;

                    if (bird != null && bird.GetComponent<CircleCollider2D>() == Physics2D.OverlapPoint(Camera.main.ScreenToWorldPoint(m_previousPosition)))
                        m_isClickOnBird = true;
                    else
                        m_isClickOnBird = false;

                    /////////////////////////////////////////////////////////////////////////////////
                    //Change to Drag state when not click on bird
                    if (!m_isClickOnBird)
                        m_state = CameraState.MOVING_UNDER_CONTROL;
                    /////////////////////////////////////////////////////////////////////////////////
                }
                break;

            case CameraState.FOLLOW_BIRD:
                if (Input.GetMouseButtonDown(0))
                {
                    m_previousPosition = Input.mousePosition;
                    m_state = CameraState.MOVING_UNDER_CONTROL;
                }
                if (previousBird.transform.position.x > m_mostLeftPosition.position.x)
                {
                    Camera.main.GetComponent<CameraViewHandled>().m_state = CameraState.AUTO_MOVING;
                    Camera.main.GetComponent<CameraViewHandled>().m_speed = 3f;
                    Camera.main.GetComponent<CameraViewHandled>().m_direction = Direction.RIGHT;
                    Camera.main.GetComponent<CameraViewHandled>().m_previousDirection = Direction.NONE;
                }
                    
                break;
            default:
                break;
        }
    }

    public IEnumerator ResetCamera(float seconds = 0f)
    {
        yield return new WaitForSeconds(seconds);

        Camera.main.GetComponent<CameraViewHandled>().m_state = CameraState.AUTO_MOVING;
        Camera.main.GetComponent<CameraViewHandled>().m_direction = Direction.LEFT;
        Camera.main.GetComponent<CameraViewHandled>().m_previousDirection = Direction.NONE;
        Camera.main.GetComponent<CameraViewHandled>().m_speed = 5f;
    }
}

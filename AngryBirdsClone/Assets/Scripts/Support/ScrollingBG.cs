﻿using UnityEngine;
using System.Collections;

public class ScrollingBG : MonoBehaviour
{
    public int m_scrollSpeed;
    private float m_scrollSpeedUnit;

    private float m_mostLeftPosition;
    private Vector3 m_resetPosition;

    private int m_numberOfElement;
    public float m_elementWidth;

    void Start()
    {
        m_scrollSpeedUnit = 0.001f;
        m_numberOfElement = GameObject.FindGameObjectsWithTag(gameObject.tag).Length;

        m_mostLeftPosition = -m_elementWidth;
        m_resetPosition = new Vector3(m_elementWidth * (m_numberOfElement - 1), transform.position.y);
    }

	void Update ()
    {
        transform.position = transform.position - Vector3.right * m_scrollSpeed * m_scrollSpeedUnit;
        if (transform.position.x <= m_mostLeftPosition)
            transform.position = m_resetPosition;
	}
}

﻿using UnityEngine;
using System.Collections;
using RyeSupport;

public class Destroyer : MonoBehaviour 
{
    void OnTriggerEnter2D(Collider2D col)
    {
        string tag = col.gameObject.tag;

        if (tag == "FlyingBird")
        {
            col.gameObject.GetComponent<Renderer>().enabled = false;
            col.gameObject.GetComponent<Collider2D>().enabled = false;
            col.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;

            Camera.main.GetComponent<CameraViewHandled>().m_state = CameraState.AUTO_MOVING;
            Camera.main.GetComponent<CameraViewHandled>().m_direction = Direction.LEFT;
            Camera.main.GetComponent<CameraViewHandled>().m_previousDirection = Direction.NONE;
            Camera.main.GetComponent<CameraViewHandled>().m_speed = 5f;
        }
        else if(col.gameObject.GetComponent<Rigidbody2D>())
            Destroy(col.gameObject);
    }
}

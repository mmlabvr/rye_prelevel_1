﻿using UnityEngine;
using System.Collections;

public class DestroyEffect : MonoBehaviour
{
    public float m_destroyTime;

	void Start ()
    {
        gameObject.GetComponent<Renderer>().sortingLayerName = "Foreground";
        gameObject.GetComponent<Renderer>().sortingOrder = 10;
	}
	

	void Update ()
    {
        Destroy(gameObject, m_destroyTime);
	}
}

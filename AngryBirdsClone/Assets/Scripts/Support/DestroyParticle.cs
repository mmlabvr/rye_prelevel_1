﻿using UnityEngine;
using System.Collections;

public class DestroyParticle : MonoBehaviour
{
	void Start ()
    {
        gameObject.GetComponent<Renderer>().sortingLayerName = "Foreground";
        gameObject.GetComponent<Renderer>().sortingOrder = 10;
	}
	

	void Update ()
    {
        if (!GetComponent<ParticleSystem>().IsAlive())
            Destroy(gameObject);
	}
}

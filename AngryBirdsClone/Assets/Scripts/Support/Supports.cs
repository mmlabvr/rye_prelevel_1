﻿namespace RyeSupport
{
	public enum PlayState
    {
        Playing,
        Pause,
        Win,
        Lose
    }

    public enum SlingshotState
    {
        IDLE,
        LINE_DRAGGING,
        WAIT_FOR_NEXT_BIRD
    }

    public enum BirdState
    {
        IDLE,
        MOVE_TO_SLINGSHOT,
        ON_SLINGSHOT,
        FLYING,
        DEATH
    }

    public enum CameraState
    {
        IDLE,
        AUTO_MOVING,
        MOVING_UNDER_CONTROL,
        FOLLOW_BIRD
    }

    public enum Direction
    {
        LEFT,
        RIGHT,
        NONE
    }

    public static class Constants
    {
        public static readonly float m_birdDragRadius = 0.5f;
        public static readonly float m_birdRealRadius = 0.098f;

        public static readonly float m_minVelocity = 0.05f;
    }

    public static class Score
    {
        public static int m_score = 0;
    }

    public static class GameState
    {
        public static PlayState m_state;
    }
}
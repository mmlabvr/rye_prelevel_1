﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using RyeSupport;

public class Controller : MonoBehaviour
{
    public Text m_score;
    public Text m_highScore;

    private GameObject m_winBoard;
    private GameObject m_loseBoard;

    private AudioSource m_win;
    private AudioSource m_lose;

    private bool flag;

    void Start()
    {
        GameState.m_state = PlayState.Playing;

        m_winBoard = GameObject.FindGameObjectWithTag("WinBoard");
        m_loseBoard = GameObject.FindGameObjectWithTag("LoseBoard");

        m_win = m_winBoard.GetComponent<AudioSource>();
        m_lose = m_loseBoard.GetComponent<AudioSource>();

        m_highScore.text = PlayerPrefs.GetInt("HighScore" + Application.loadedLevelName, 0).ToString();

        flag = false;
    }

    void Update()
    {
        m_score.text = Score.m_score.ToString();

        if (IsStopMoving())
        {
            if (GameObject.FindGameObjectsWithTag("Pig").Length == 0)
                GameState.m_state = PlayState.Win;
            else if (GameObject.FindGameObjectsWithTag("Bird").Length == 0)
                GameState.m_state = PlayState.Lose;
        }

        if (!flag)
        {
            if (GameState.m_state == PlayState.Win)
            {
                StartCoroutine(Camera.main.GetComponent<CameraZoom>().AutoZoomIn());
                StartCoroutine(ReadyForScoreBoard());

                flag = true;
            }
            else if (GameState.m_state == PlayState.Lose)
            {
                StartCoroutine(DisplayLoseBoard());

                flag = true;
            }
        }
    }

    public static void AutoResize(int screenWidth, int screenHeight)
    {
        Vector2 resizeRatio = new Vector2((float)Screen.width / screenWidth, (float)Screen.height / screenHeight);
        GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(resizeRatio.x, resizeRatio.y, 1.0f));
    }

    void OnGUI()
    {
        AutoResize(800, 480);
    }

    bool IsStopMoving()
    {
        foreach(GameObject target in FindObjectsOfType(typeof(GameObject)))
        {
            if (target.tag == "Bird")
            {
                if (target.GetComponent<Bird>().m_state == BirdState.DEATH)
                    return false;

                if (target.GetComponent<Bird>().m_state != BirdState.FLYING)
                    continue;
            }
            if (target.GetComponent<Rigidbody2D>() != null && target.GetComponent<Rigidbody2D>().velocity.magnitude > 0)
                return false;
        }

        return true;
    }

    IEnumerator ReadyForScoreBoard()
    {
        yield return new WaitForSeconds(4f);
        GameObject[] birdList = GameObject.FindGameObjectsWithTag("Bird");

        for (int i = 0; i < birdList.Length; i++)
        {
            Score.m_score += 10000;
            birdList[i].GetComponent<BirdAnimation>().Scored();
            yield return new WaitForSeconds(1f);
        }

        m_win.Play();

        m_winBoard.GetComponent<Canvas>().enabled = true;
        m_winBoard.GetComponent<ScoreBoardHandled>().enabled = true;
        m_winBoard.GetComponent<AudioSource>().Play();

        DisableAllOtherEffects();
    }

    IEnumerator DisplayLoseBoard()
    {
        yield return new WaitForSeconds(3f);

        m_lose.Play();
        m_loseBoard.GetComponent<Canvas>().enabled = true;

        DisableAllOtherEffects();
    }

    void DisableAllOtherEffects()
    {
        Camera.main.GetComponent<CameraViewHandled>().enabled = false;
        GameObject.FindGameObjectWithTag("MainUI").SetActive(false);

        GameObject[] birdList = GameObject.FindGameObjectsWithTag("Bird");

        for (int i = 0; i < birdList.Length; i++)
        {
            birdList[i].GetComponent<BirdAnimation>().enabled = false;
        }
    }
}

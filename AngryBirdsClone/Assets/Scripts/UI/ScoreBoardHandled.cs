﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using RyeSupport;

public class ScoreBoardHandled : MonoBehaviour 
{
    public Text m_score;
    private bool m_isNewHighScore;

    public GameObject m_star1;
    public GameObject m_star2;
    public GameObject m_star3;

    public GameObject m_birdForHighScore;
    public GameObject m_bestScoreText;

    public int m_scoreForSecondStar;
    public int m_scoreForThirdStar;

    void OnEnable()
    {
        m_score.text = Score.m_score.ToString();
        int highScore = PlayerPrefs.GetInt("HighScore" + Application.loadedLevelName, 0);

        if (Score.m_score <= highScore)
        {
            m_bestScoreText.GetComponent<Text>().text = "Best: " + highScore.ToString();
            m_isNewHighScore = false;
        }
        else
        {
            m_bestScoreText.GetComponent<Text>().text = "New best: " + Score.m_score;
            PlayerPrefs.SetInt("HighScore" + Application.loadedLevelName, Score.m_score);
            m_isNewHighScore = true;

        }

        StartCoroutine(Reward(1f));
    }

    IEnumerator Reward(float seconds)
    {
        yield return new WaitForSeconds(2f);

        m_star1.SetActive(true);
        yield return new WaitForSeconds(seconds);

        if (Score.m_score >= m_scoreForSecondStar)
        {
            m_star2.SetActive(true);
            yield return new WaitForSeconds(seconds);
        }
        if(Score.m_score >= m_scoreForThirdStar)
        {
            m_star3.SetActive(true);
            yield return new WaitForSeconds(seconds);
        }

        m_bestScoreText.SetActive(true);
        if(m_isNewHighScore)
        {
            m_birdForHighScore.SetActive(true);
            Camera.main.GetComponent<CameraShake>().Shake();
        }
    }
}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using RyeSupport;

public class LoadLevelByName : MonoBehaviour
{
    public Text level;

    void Start()
    {
        level.text = FindLevelText();
    }

	public void LevelButtonClick()
    {
        if (Application.CanStreamedLevelBeLoaded(gameObject.GetComponent<Button>().name))
            Application.LoadLevel(gameObject.GetComponent<Button>().name);
        else
            Debug.Log(gameObject.GetComponent<Button>().name + " is not exists");

        Score.m_score = 0;
    }

    string FindLevelText()
    {
        string levelName = gameObject.name;

        int i = 0;
        while (levelName[i++] != '-');

        string text = null;
        while(i < levelName.Length)
        {
            text += levelName[i++];
        }

        return text;
    }
}

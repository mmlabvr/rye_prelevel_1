﻿using UnityEngine;
using System.Collections;
using RyeSupport;

public class UIButton : MonoBehaviour
{
    private GameObject m_pauseMenu;

    void Start()
    {
        m_pauseMenu = GameObject.Find("PauseMenu");
    }

    public void PauseButtonClick()
    {
        Camera.main.GetComponent<CameraViewHandled>().enabled = false;

        m_pauseMenu.GetComponent<Animator>().Play("moveRight");
        Time.timeScale = 0f;
    }

    public void ResumeButtonClick()
    {
        Camera.main.GetComponent<CameraViewHandled>().enabled = true;

        m_pauseMenu.GetComponent<Animator>().Play("moveLeft");
        Time.timeScale = 1f;
    }

    public void ZoomInButtonClick()
    {
        Camera.main.GetComponent<CameraZoom>().ZoomIn();
    }

    public void ZoomOutButtonClick()
    {
        Camera.main.GetComponent<CameraZoom>().ZoomOut();
    }

    public void ReplayButtonClick()
    {
        Application.LoadLevel(Application.loadedLevel);
        Time.timeScale = 1f;

        Score.m_score = 0;
    }

    public void MenuButtonClick()
    {
        Application.LoadLevel("Menu");
        Time.timeScale = 1f;
    }

    public void NextButtonClick()
    {
        string nextLevel = FindNextLevel(Application.loadedLevelName);

        if (Application.CanStreamedLevelBeLoaded(nextLevel))
            Application.LoadLevel(nextLevel);
        else
        {
            Debug.Log(nextLevel + " is not exists");
            Application.LoadLevel("Menu");
        }

        Time.timeScale = 1f;
        Score.m_score = 0;
    }

    public string FindNextLevel(string currentLevel)
    {
        string nextLevel = null;

        int i;
        for (i = currentLevel.Length - 1; ; i--)
        {
            if (currentLevel[i] == '-')
                break;
        }

        nextLevel = currentLevel.Substring(0, i + 1) + (int.Parse(currentLevel.Substring(i + 1, currentLevel.Length - i - 1)) + 1).ToString();
        return nextLevel;
    }
}
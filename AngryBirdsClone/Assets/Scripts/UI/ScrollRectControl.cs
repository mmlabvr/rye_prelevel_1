﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ScrollRectControl : MonoBehaviour
{
    private ScrollRect m_scrollPanel;
    public int m_pages;
    private int m_currentPage;

    private Vector3 m_mouseDownPosition;
    private Vector3 m_previousPosition;

    private float m_dragTime;
    private float m_lastTime;

    void Start()
    {
        m_scrollPanel = GetComponent<ScrollRect>();
        m_currentPage = 1;

        m_dragTime = 1f;
    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_previousPosition = Input.mousePosition;
            m_mouseDownPosition = Input.mousePosition;

            m_lastTime = Time.time;
        }

        if (Input.GetMouseButton(0))
            Dragging();
        else if (Input.GetMouseButtonUp(0))
            Dropping();

        if(!Input.GetMouseButton(0))
        {
            Vector3 cameraPosition = Camera.main.WorldToScreenPoint(Camera.main.transform.position);
            Vector3 position = new Vector3(Camera.main.pixelWidth * (m_pages - m_currentPage), cameraPosition.y, m_scrollPanel.transform.position.z);
            m_scrollPanel.content.position = Vector3.Lerp(m_scrollPanel.content.position, Camera.main.ScreenToWorldPoint(position), 5f * Time.deltaTime);
        }
            
    }

    void Dragging()
    {
        if (Time.time - m_lastTime >= m_dragTime)
        {
            m_lastTime = Time.time;
            m_previousPosition = Input.mousePosition;
        }
    }

    void Dropping()
    {
        if (Input.mousePosition.x - m_mouseDownPosition.x >= 400f || Input.mousePosition.x - m_previousPosition.x >= 50f)
        {
            if (m_currentPage > 1)
                m_currentPage--;
        }
        else if (m_mouseDownPosition.x - Input.mousePosition.x >= 400f || m_previousPosition.x - Input.mousePosition.x >= 50f)
        {
            if (m_currentPage < m_pages)
                m_currentPage++;
        }
    }
}

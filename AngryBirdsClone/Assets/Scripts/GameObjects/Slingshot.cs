﻿using UnityEngine;
using System.Collections;
using RyeSupport;

public class Slingshot : MonoBehaviour
{
    public GameObject m_left;
    public GameObject m_right;
    private LineRenderer m_leftLineRenderer;
    private LineRenderer m_rightLineRenderer;

    public Transform m_waitPosition;
    private Vector3 m_slingshotCentralPosition;
    public GameObject m_holder;

    private float m_previousDragDistance;
    private float m_elapseTime;

    private const float m_maxStretch = 0.7f;
    private SlingshotState m_state;

    [HideInInspector]
    public GameObject m_bird;
    [HideInInspector]
    public GameObject m_previousBird;

    //Determine bird_position
    private Ray m_slingshotRay;
    private AudioSource[] m_audios;

    void Start()
    {
        m_slingshotCentralPosition = (m_left.transform.position + m_right.transform.position) / 2;
        m_slingshotRay = new Ray(m_slingshotCentralPosition, Vector3.zero);

        m_previousDragDistance = 0;
        m_elapseTime = 0;

        SetUpLineRenderer();
        m_audios = GetComponents<AudioSource>();

        m_state = SlingshotState.WAIT_FOR_NEXT_BIRD;
    }

    void Update()
    {
        if (GameState.m_state == PlayState.Playing)
        {
            switch (m_state)
            {
                case SlingshotState.IDLE:
                    if (m_bird == null)
                        return;

                    if (m_bird.GetComponent<Bird>().m_state == BirdState.MOVE_TO_SLINGSHOT)
                        m_bird.GetComponent<Bird>().MoveTo(m_waitPosition.position);
                    else if (Input.GetMouseButtonDown(0))
                    {
                        //Click on bird
                        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                        if (m_bird.GetComponent<CircleCollider2D>() == Physics2D.OverlapPoint(mousePosition))
                            m_state = SlingshotState.LINE_DRAGGING;
                    }
                    break;

                case SlingshotState.LINE_DRAGGING:
                    if (Input.GetMouseButton(0))
                        Dragging();
                    else
                    {
                        Vector2 distance = m_bird.transform.position - m_slingshotCentralPosition;

                        if (distance.magnitude < 0.3f)
                        {
                            m_holder.transform.position = new Vector3(
                                                                        m_waitPosition.transform.position.x - Constants.m_birdRealRadius / 2,
                                                                        m_waitPosition.transform.position.y - Constants.m_birdRealRadius / 2, 
                                                                        m_waitPosition.transform.position.z
                                                                     );

                            m_bird.GetComponent<Bird>().m_state = BirdState.MOVE_TO_SLINGSHOT;
                            m_state = SlingshotState.IDLE;
                        }
                        else
                        {
                            if (m_previousBird)
                                Destroy(m_previousBird);

                            m_previousBird = m_bird;
                            m_bird = null;
                            m_previousBird.tag = "FlyingBird";

                            m_previousBird.GetComponent<Bird>().Shoot(distance.magnitude, m_slingshotCentralPosition);

                            m_audios[Random.Range(1, 6)].Play();
                            m_audios[Random.Range(7, 9)].Play();

                            m_holder.transform.position = new Vector3(
                                                                        m_waitPosition.transform.position.x - Constants.m_birdRealRadius / 2,
                                                                        m_waitPosition.transform.position.y - Constants.m_birdRealRadius / 2,
                                                                        m_waitPosition.transform.position.z
                                                                     );

                            Camera.main.GetComponent<CameraViewHandled>().m_state = CameraState.FOLLOW_BIRD;
                            m_state = SlingshotState.WAIT_FOR_NEXT_BIRD;
                        }
                    }
                    break;

                case SlingshotState.WAIT_FOR_NEXT_BIRD:
                    StartCoroutine(FindNextBirdAfter(1f));
                    m_state = SlingshotState.IDLE;

                    break;
                default:
                    break;
            }
        }
        else if (m_previousBird)
            Destroy(m_previousBird);

        RenderLine(m_holder.transform.position);
    }

    void SetUpLineRenderer()
    {
        m_leftLineRenderer = m_left.GetComponent<LineRenderer>();
        m_rightLineRenderer = m_right.GetComponent<LineRenderer>();

        m_leftLineRenderer.sortingLayerName = m_left.GetComponent<Renderer>().sortingLayerName;
        m_rightLineRenderer.sortingLayerName = m_right.GetComponent<Renderer>().sortingLayerName;

        m_leftLineRenderer.sortingOrder = m_left.GetComponent<Renderer>().sortingOrder - 1;
        m_rightLineRenderer.sortingOrder = m_right.GetComponent<Renderer>().sortingOrder - 1;

        m_leftLineRenderer.SetPosition(0, m_left.transform.position);
        m_rightLineRenderer.SetPosition(0, m_right.transform.position);
    }

    void RenderLine(Vector3 secondPosition)
    {
        m_leftLineRenderer.SetPosition(1, secondPosition);
        m_rightLineRenderer.SetPosition(1, secondPosition);
    }

    Vector3 GetPoint(Ray direction, float distance, float maxStrech)
    {
        Vector3 returnPoint = Vector3.zero;

        float realDistance;
        if (distance > maxStrech)
            realDistance = maxStrech;
        else
            realDistance = distance;

        returnPoint = direction.GetPoint(realDistance);

        return returnPoint;
    }

    void Dragging()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 currentDirection = mousePosition - m_slingshotCentralPosition;

        m_slingshotRay.direction = currentDirection;
        m_bird.transform.position = GetPoint(m_slingshotRay, currentDirection.magnitude, m_maxStretch);

        m_holder.transform.position = GetPoint(
                                                m_slingshotRay,
                                                currentDirection.magnitude + Constants.m_birdRealRadius,
                                                m_maxStretch + Constants.m_birdRealRadius
                                              );

        float holderAngle = Mathf.Atan2(currentDirection.y, currentDirection.x) * Mathf.Rad2Deg;
        m_holder.transform.rotation = Quaternion.AngleAxis(180 - holderAngle, Vector3.back);

        Vector2 distance = m_bird.transform.position - m_slingshotCentralPosition;

        m_elapseTime += Time.deltaTime;
        if (m_elapseTime >= 0.5f && !m_audios[0].isPlaying)
        {
            if (distance.magnitude - m_previousDragDistance > 0.2f)
                m_audios[0].Play();

            m_previousDragDistance = distance.magnitude;
            m_elapseTime = 0;
        }
    }

    GameObject FindNextBird()
    {
        GameObject[] listBird = GameObject.FindGameObjectsWithTag("Bird");
        GameObject returnBird = null;
        if (listBird.Length != 0)
        {
            returnBird = listBird[0];
            foreach (GameObject bird in listBird)
            {
                if (bird.transform.position.x > returnBird.transform.position.x)
                    returnBird = bird;
            }
        }

        if(returnBird)
        {
            returnBird.GetComponent<Bird>().m_state = BirdState.MOVE_TO_SLINGSHOT;
            returnBird.GetComponent<Collider2D>().enabled = false;
            returnBird.GetComponent<Rigidbody2D>().isKinematic = true;

            returnBird.GetComponent<BirdAnimation>().m_audios[Random.Range(12, 15)].Play();
        }

        return returnBird;
    }

    IEnumerator FindNextBirdAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        m_bird = FindNextBird();
    }

    IEnumerator PlayBirdNextMilitary(float second)
    {
        yield return new WaitForSeconds(second);
        m_audios[Random.Range(10, 12)].Play();
    }
}
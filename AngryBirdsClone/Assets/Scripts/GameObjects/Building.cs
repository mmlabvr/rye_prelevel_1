﻿using UnityEngine;
using System.Collections;
using RyeSupport;

public class Building : MonoBehaviour
{
    [HideInInspector]
    public bool m_isAlive;

	void Start ()
    {
        m_isAlive = true;
	}
	
	void Update ()
    {
        if (!GetComponent<Health>().m_isAlive)
            StartCoroutine(DestroyAfter(0.5f));
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Rigidbody2D>())
        {
            if (col.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude > 0.05f)
            {
                gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
				//gameObject.rigidbody2D.velocity = col.gameObject.rigidbody2D.velocity;
			}
        }
    }

    IEnumerator DestroyAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        Score.m_score += 500;
        Destroy(gameObject);
    }
}

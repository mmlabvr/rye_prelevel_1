﻿using UnityEngine;
using System.Collections;
using RyeSupport;

public class Bird : MonoBehaviour
{
    //[HideInInspector]
    public BirdState m_state;

	void Start ()
    {
        m_state = BirdState.IDLE;
        GetComponent<TrailRenderer>().enabled = false;
	}
	
	void Update ()
    {
        if (m_state == BirdState.DEATH)
        {
            StartCoroutine(Camera.main.GetComponent<CameraViewHandled>().ResetCamera(1f));
            StartCoroutine(InactiveAfter(1f));
        }
	}

    IEnumerator InactiveAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        GetComponent<Renderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;
        GetComponent<Rigidbody2D>().isKinematic = true;
    }

    public void MoveTo(Vector3 position)
    {
        transform.position = Vector3.Lerp(transform.position, position, 0.25f);

        if ((transform.position - position).magnitude <= 0.2f)
        {
            GetComponent<Collider2D>().enabled = true;
            GetComponent<CircleCollider2D>().radius = Constants.m_birdDragRadius;
            GetComponent<CircleCollider2D>().isTrigger = true;
            GetComponent<BirdAnimation>().m_isOnSlingshot = true;
			GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;

            transform.position = position;
            m_state = BirdState.ON_SLINGSHOT;
        }
    }

    public void Shoot(float distance, Vector3 slingshotPosition)
    {
        Vector3 direction = slingshotPosition - transform.position;

        GetComponent<Rigidbody2D>().isKinematic = false;
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
		GetComponent<Rigidbody2D>().gravityScale = 0.2f;
        GetComponent<Rigidbody2D>().velocity = direction * distance * 10;

        GetComponent<CircleCollider2D>().radius = Constants.m_birdRealRadius;
        GetComponent<CircleCollider2D>().isTrigger = false;

        GetComponent<TrailRenderer>().enabled = true;
        GetComponent<TrailRenderer>().sortingLayerName = "Foreground";
        GetComponent<TrailRenderer>().sortingOrder = 3;

        m_state = BirdState.FLYING;
    }
}
﻿using UnityEngine;
using System.Collections;
using RyeSupport;

public class Health : MonoBehaviour
{
    public float m_maxHealth;
    [HideInInspector]
    public float m_health;

    [HideInInspector]
    public bool m_isAlive;
    [HideInInspector]
    public bool m_isIdle;

	void Start () {
        m_isAlive = true;
        m_isIdle = true;

        m_health = m_maxHealth;
	}
	
	void Update ()
    {
        if (gameObject.tag == "FlyingBird")
            m_health -= 0.3f;
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if(gameObject.tag != "Bird")
            m_isIdle = false;
    }

    void OnCollisionStay2D(Collision2D col)
    {
        if (!m_isIdle)
        {
            Vector2 thisObjectVeloc = GetComponent<Rigidbody2D>() ? GetComponent<Rigidbody2D>().velocity : Vector2.zero;
            Vector2 collisionObjectVeloc = col.gameObject.GetComponent<Rigidbody2D>() ? col.gameObject.GetComponent<Rigidbody2D>().velocity : Vector2.zero;

            float damage = (thisObjectVeloc - collisionObjectVeloc).magnitude;

            if (m_health > 0)
            {
                if (damage > 0)
                    m_health -= damage;
                else if (gameObject.tag == "FlyingBird")
                    m_health -= 0.5f;
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class CreateFlyingBird : MonoBehaviour
{
    public GameObject m_bird;
    private float m_elapseTime;

    void Start() 
    {
        m_bird.SetActive(false);

        m_elapseTime = 0f;
    }

	void Update () 
    {
        m_elapseTime += Time.deltaTime;

        if(m_elapseTime > 4f && m_bird.activeInHierarchy == false)
        {
            SetActiveBird();
            m_elapseTime = 0f;
        }

        if (m_bird.transform.position.y < -3f)
            m_bird.SetActive(false);
	}

    void SetActiveBird()
    {
        m_bird.SetActive(true);

        float scale = (float)Random.Range(1, 6) / 2;
        m_bird.transform.position = new Vector3(Random.Range(-12, 3), 0);
        m_bird.transform.localScale = new Vector3(scale, scale);

        m_bird.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(200, 400), Random.Range(200, 400)));
    }
}
﻿using UnityEngine;
using System.Collections;
using RyeSupport;

public class PigAnimation : MonoBehaviour 
{
    private Animator m_animator;

    private Object m_smokePrefab;
    private Object m_scorePrefab;

    private AudioSource[] m_audios;

    private float m_elapseTime;
    private float m_actionTime;

    private int m_damageLevel;

	void Start ()
    {
        m_smokePrefab = Resources.Load("Prefabs/Effects/EnemySmoke");
        m_scorePrefab = Resources.Load("Prefabs/Effects/Scores/5000");

        m_animator = GetComponent<Animator>();
        m_damageLevel = m_animator.GetInteger("DamageLevel");
        m_audios = GetComponents<AudioSource>();

        m_actionTime = 1f;
        m_elapseTime = 0;
	}

	void Update ()
    {
        m_elapseTime += Time.deltaTime;

        if(m_elapseTime >= m_actionTime)
        {
            if(GetComponent<Renderer>().isVisible && m_damageLevel == 0)
                m_audios[Random.Range(0, 9)].Play();

            m_elapseTime = 0;
        }

        float health = GetComponent<Health>().m_health;
        float maxHealth = GetComponent<Health>().m_maxHealth;
        if (GetComponent<Health>().m_isAlive && health <= 0)
        {
            m_audios[26].Play();

            GameObject smoke = Instantiate(m_smokePrefab) as GameObject;

            smoke.GetComponent<Renderer>().sortingLayerName = gameObject.GetComponent<Renderer>().sortingLayerName;
            smoke.GetComponent<Renderer>().sortingOrder = gameObject.GetComponent<Renderer>().sortingOrder;
            smoke.transform.position = gameObject.transform.position;

            Score.m_score += 5000;
            GameObject score = Instantiate(m_scorePrefab, transform.position, new Quaternion()) as GameObject;

            GetComponent<Health>().m_isAlive = false;
        }
        else if (health / maxHealth <= 0.25f)
            m_damageLevel = 2;
        else if (health / maxHealth <= 0.75f)
            m_damageLevel = 1;
        else
            m_damageLevel = 0;

        m_animator.SetInteger("DamageLevel", m_damageLevel);
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Rigidbody2D>())
        {
            if (col.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude > 0.05f)
            {
                gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
                gameObject.GetComponent<Rigidbody2D>().velocity = col.gameObject.GetComponent<Rigidbody2D>().velocity;
            }
        }

        if (GetComponent<Health>().m_health / GetComponent<Health>().m_maxHealth >= 0.8f)
        {
            for (int i = 10; i <= 17; i++)
            {
                if (m_audios[i].isPlaying)
                    return;
            }
            m_audios[Random.Range(10, 17)].Play();
        }
        else
        {
            for (int i = 18; i <= 25; i++)
            {
                if (m_audios[i].isPlaying)
                    return;
            }
            m_audios[Random.Range(18, 25)].Play();
        }
    }
}

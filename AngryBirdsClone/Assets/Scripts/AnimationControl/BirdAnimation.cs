﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RyeSupport;

public class BirdAnimation : MonoBehaviour {

    [HideInInspector]
    public Animator m_animator;

    private Object m_smokePrefab;
    private Object m_scorePrefab;
    private List<Object> m_featherPrefabs;

    [HideInInspector]
    public AudioSource[] m_audios;
    [HideInInspector]
    public bool m_isOnSlingshot;

    private float m_elapseTime;
    private float m_actionTime;

	int random;

	void Start () {
        m_animator = GetComponent<Animator>();

        m_smokePrefab = Resources.Load("Prefabs/Effects/BirdSmoke");
        m_scorePrefab = Resources.Load("Prefabs/Effects/Scores/10000");
        m_featherPrefabs = new List<Object>();
        for (int i = 0; i < 2; i++)
        {
            Object feather = Resources.Load("Prefabs/Effects/Feather" + (i + 1));
            m_featherPrefabs.Add(feather);
        }

        m_audios = GetComponents<AudioSource>();
        m_isOnSlingshot = false;

        SetActionTime();
        m_elapseTime = 0;
	}
	
	void Update () {
        m_elapseTime += Time.deltaTime;

        if(m_animator.GetCurrentAnimatorStateInfo(0).IsName("birdIdle"))
        {
            m_animator.SetBool("isWink", false);
            m_animator.SetBool("isSing", false);
        }

        if (m_elapseTime >= m_actionTime)
        {
			if (gameObject.GetComponent<Rigidbody2D>().velocity.magnitude <= 0.05f && gameObject.GetComponent<Bird>().m_state == BirdState.IDLE)
			{
				gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
				transform.rotation = Quaternion.identity;
			}

            random = Random.Range(0, 10);

            if (random < 3)
            {
                m_animator.SetBool("isWink", true);
            }
            else if(random < 7)
            {
                m_animator.SetBool("isSing", true);
            }

			random = Random.Range(0, 10);

			if (random < 7 && GetComponent<Bird>().m_state == BirdState.IDLE)
			{
				gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
				gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, Random.Range(30, 100)));
			}

			m_audios[Random.Range(0, 11)].Play();
            m_elapseTime = 0;
        }

		if (GetComponent<Bird>().m_state == BirdState.IDLE && random < 3)
			gameObject.transform.RotateAround(transform.position, Vector3.forward, 360.0f * Time.deltaTime);

		if (GetComponent<Health>().m_isAlive && GetComponent<Health>().m_health <= 0)
        {
            GetComponent<Bird>().m_state = BirdState.DEATH;
            GetComponent<Health>().m_isAlive = false;

            StartCoroutine(DestroyEffect());
        }
	}

    public void SetActionTime()
    {
        if (m_isOnSlingshot)
            m_actionTime = 2f;
        else
            m_actionTime = 1f;
    }


    IEnumerator DestroyEffect()
    {
        yield return new WaitForSeconds(1f);

        m_audios[16].Play();

        DoSmokeEffect();
        FeatherFlying();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (GetComponent<Bird>().m_state == BirdState.FLYING)
        {
            Vector2 velocity = GetComponent<Rigidbody2D>().velocity;
            Vector2 colVelocity = col.gameObject.GetComponent<Rigidbody2D>() ? col.gameObject.GetComponent<Rigidbody2D>().velocity : Vector2.zero;

            if ((velocity + colVelocity).magnitude > 3f)
            {
                DoSmokeEffect();
                FeatherFlying();
            }
        }
    }

    void DoSmokeEffect()
    {
        GameObject smoke = Instantiate(m_smokePrefab) as GameObject;

        smoke.GetComponent<Renderer>().sortingLayerName = gameObject.GetComponent<Renderer>().sortingLayerName;
        smoke.GetComponent<Renderer>().sortingOrder = gameObject.GetComponent<Renderer>().sortingOrder;
        smoke.transform.position = gameObject.transform.position;
    }

    void FeatherFlying()
    {
        GameObject[] feather = new GameObject[Random.Range(1, 5)];
        for (int i = 0; i < feather.Length; i++)
        {
            feather[i] = Instantiate(m_featherPrefabs[Random.Range(0, m_featherPrefabs.Count)], transform.position, transform.rotation) as GameObject;

            feather[i].GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(-1f, 1f), Random.Range(0f, 0.5f));
            feather[i].GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-30f, 30f);
        }
    }

    public void Scored()
    {
        GameObject score = Instantiate(m_scorePrefab, transform.position, new Quaternion()) as GameObject;
    }
}
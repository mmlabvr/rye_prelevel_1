﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildingAnimation : MonoBehaviour
{
    private Animator m_animator;
    private int m_damageLevel;

    private AudioSource[] m_audios;

    private List<Object> m_breakPartPrefabs;

    void Start()
    {
        m_animator = GetComponent<Animator>();
        m_audios = GetComponents<AudioSource>();

        m_breakPartPrefabs = new List<Object>();

        if (tag == "Wood")
        {
            for (int i = 0; i < 3; i++)
            {
                Object wood = Resources.Load("Prefabs/Building/BreakPart/Wood" + (i + 1));
                m_breakPartPrefabs.Add(wood);
            }
        }
        else if (tag == "Ice")
        {
            for (int i = 0; i < 5; i++)
            {
                Object ice = Resources.Load("Prefabs/Building/BreakPart/Ice" + (i + 1));
                m_breakPartPrefabs.Add(ice);
            }
        }
        else if (tag == "Rock")
        {
            for (int i = 0; i < 3; i++)
            {
                Object rock = Resources.Load("Prefabs/Building/BreakPart/Rock" + (i + 1));
                m_breakPartPrefabs.Add(rock);
            }
        }
    }


    void Update()
    {
        float health = GetComponent<Health>().m_health;
        float maxHealth = GetComponent<Health>().m_maxHealth;

        if (GetComponent<Health>().m_isAlive && health <= 0)
        {
            GetComponent<Renderer>().enabled = false;
            GetComponent<Collider2D>().enabled = false;

            //BreakingObject();

            GetComponent<Health>().m_isAlive = false;
        }
        else if (health / maxHealth <= 0.25f)
            m_damageLevel = 3;
        else if (health / maxHealth <= 0.5f)
            m_damageLevel = 2;
        else if (health / maxHealth <= 0.75f)
            m_damageLevel = 1;
        else
            m_damageLevel = 0;

        m_animator.SetInteger("DamageLevel", m_damageLevel);
    }

    void BreakingObject()
    {
        GameObject[] breakPart = new GameObject[Random.Range(3, 8)];

        for (int i = 0; i < breakPart.Length; i++)
        {
            breakPart[i] = Instantiate(m_breakPartPrefabs[Random.Range(0, m_breakPartPrefabs.Count)], transform.position, transform.rotation) as GameObject;

            breakPart[i].GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(-1f, 1f), Random.Range(0f, 0.5f));
            breakPart[i].GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-30f, 30f);
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (m_damageLevel == 1)
        {
            if (tag == "Wood")
            {
                for (int i = 0; i <= 2; i++)
                {
                    if (m_audios[i].isPlaying)
                        return;
                }
                m_audios[Random.Range(0, 2)].Play();
            }
            else if (tag == "Ice")
            {
                for (int i = 0; i <= 7; i++)
                {
                    if (m_audios[i].isPlaying)
                        return;
                }
                m_audios[Random.Range(0, 7)].Play();
            }
            else if (tag == "Rock")
            {
                for (int i = 0; i <= 4; i++)
                {
                    if (m_audios[i].isPlaying)
                        return;
                }
                m_audios[Random.Range(0, 4)].Play();
            }
        }
        else if (m_damageLevel == 2 || m_damageLevel == 3)
        {
            if (tag == "Wood")
            {
                if (m_audios[3].isPlaying)
                    return;
                m_audios[3].Play();
            }
            else if (tag == "Ice")
            {
                for (int i = 8; i <= 10; i++)
                {
                    if (m_audios[i].isPlaying)
                        return;
                }
                m_audios[Random.Range(8, 10)].Play();
            }
            else if (tag == "Rock")
            {
                for (int i = 5; i <= 7; i++)
                {
                    if (m_audios[i].isPlaying)
                        return;
                }
                m_audios[Random.Range(5, 7)].Play();
            }
        }
        else if (m_damageLevel == 4)
        {
            if (tag == "Wood")
            {
                if (m_audios[4].isPlaying)
                    return;
                m_audios[4].Play();
            }
            else if (tag == "Ice")
            {
                for (int i = 11; i <= 13; i++)
                {
                    if (m_audios[i].isPlaying)
                        return;
                }
                m_audios[Random.Range(11, 13)].Play();
            }
            else if (tag == "Rock")
            {
                for (int i = 8; i <= 10; i++)
                {
                    if (m_audios[i].isPlaying)
                        return;
                }
                m_audios[Random.Range(8, 10)].Play();
            }
        }

        if(col.gameObject.tag == "Bird")
            BreakingObject();
    }
}